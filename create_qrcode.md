# Make a qrcode

As a test, make a qrcode for this url: https://mmbhof.org/the-museum/visit/

```sh
qrencode -o visit.png -d 300 https://mmbhof.org/the-museum/visit/
```

It should produce a [qrcode png](images/visit.png). 
