import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { Ionicons } from "@expo/vector-icons";

import Scan from "../screens/Scan";
import Bike from "../screens/Bike";
import Record from "../screens/Record";

//render a navigation component with the Scan and Bike screens
export default function Navigation() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Scan" component={Scan} />
        <Stack.Screen name="Bike" component={Bike} />
        <Stack.Screen name="Record" component={Record} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
