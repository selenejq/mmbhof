# Marin Museum of Biking and Mountain Bike Hall of Fame

[mmbhof](https://gitlab.com/andrew-gitlab/mmbhof.git)

> The Marin Museum of Bicycling is a premier bicycle history museum and cultural center in Fairfax, California. We display key examples from the 19th century Golden Age of Cycling, we are home to the Mountain Bike Hall of Fame, showcasing the birth and evolution of the mountain bike and we feature many more bicycles: road, track, racing, touring and everyday transportation.

# Thoughts on a Tour App

These are just some thoughts on how we can help the museum "go mobile". Feel free to send a patch, merge request, or ideas. 

## Current State

Currently the museum relies on descriptive placards and volunteer tour guides. Often, the tour guides were involved at the beginning of the mountain bike so they have a lot of knowledge abot their experiences. They also love bicycles and know a ton of info about everything in the museum. Your chance visit on any day may get one of these volunteers, or may have no volunteers so the visitor just read the placards and moves on. 

### Possible Directions

1. Review possible existing museum tour apps for iOS and Android. (See #2)
1. Review and Compare existing museum tour apps on dedicated devices (phones, tablets, etc) checked out from a Front Desk. (also #2)
1. What if we just used a browser engine, wrapped in a theme of the MMBHOF?
1. QRCode all the things!  For each bicycle or "station" at the museum, a QRCode provides a link to the content (text, images, sound, video). 
1. Could we follow the various volunteer tour guides around and record their audio tour? Then break these audio recordings into bite sized chunks to be hosted at a web page?
1. Could we use station numbers/names and an NFC code at each? Visitors scan the station ID and are taken to the content (text, images, sound, video).
1. We need something easy to maintain and update, assuming the content changes, as do the bicycles, over time.
1. How to accommodate vision or hearing impaired visitors?
1. How to handle people who want to hear the content? text to speech or record a volunteer?
1. How to handle larger format devices like tablets or laptops?
1. Visitor wants to share their visit, do we have pre-defined hashtags or locations great for sharing?

## User Stories

What's a user story? Read about them from [Agile Alliance](https://www.agilealliance.org/glossary/user-stories/). 

1. Visitor wants to learn more about the bicycle in front of them. They borrow a tablet from the Front Desk and scan a QRCode. This QRCode takes them to the content (text, images, audio, video).
1. The content is all web-accessible, so the visitor scans a QRCode/Station ID on their phone and are taken to the content. 
1. Visitor wants to hear a history instead of reading it using their device.
1. Visitor wants to hear a history instead of reading it using the museum tablet.
1. Visitor wants a scavenger hunt. The app prompts to find certain bikes or parts in various places in the museum. 
