import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Audio, AVPlaybackStatus } from "expo-av";
import * as FileSystem from "expo-file-system";
import { Ionicons } from "@expo/vector-icons";
import Permissions from "expo-permissions";
import Slider from "@react-native-community/slider";

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
const MAIN_COLOR = "#FC6351";

type Props = {};

type State = {
  hasRecordingPermissions: boolean;
  isLoading: boolean;
  isPlaybackAllowed: boolean;
  muted: boolean;
  soundPosition: number | null;
  soundDuration: number | null;
  recordingDuration: number | null;
  shouldPlay: boolean;
  isPlaying: boolean;
  isRecording: boolean;
  fontLoaded: boolean;
  shouldCorrectPitch: boolean;
  volume: number;
  rate: number;
};

export default class Record extends React.Component<Props, State> {
  private recording: Audio.Recording | null;
  private sound: Audio.Sound | null;
  private isSeeking: boolean;
  private shouldPlayAtEndOfSeek: boolean;
  private readonly recordingSettings: Audio.RecordingOptions;

  constructor(props: Props) {
    super(props);
    this.recording = null;
    this.sound = null;
    this.isSeeking = false;
    this.shouldPlayAtEndOfSeek = false;
    this.state = {
      hasRecordingPermissions: false,
      isLoading: false,
      isPlaybackAllowed: false,
      muted: false,
      soundPosition: null,
      soundDuration: null,
      recordingDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isRecording: false,
      fontLoaded: false,
      shouldCorrectPitch: true,
      volume: 1.0,
      rate: 1.0,
    };
    this.recordingSettings = Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY;
  }

  componentDidMount() {
    this._askForPermissions();
  }

  private _askForPermissions = async () => {
    const response = await Audio.requestPermissionsAsync();
    this.setState({
      hasRecordingPermissions: response.status === "granted",
    });
  };

  private _updateScreenForSoundStatus = (status: AVPlaybackStatus) => {
    if (status.isLoaded) {
      this.setState({
        soundDuration: status.durationMillis ?? null,
        soundPosition: status.positionMillis,
        shouldPlay: status.shouldPlay,
        isPlaying: status.isPlaying,
        rate: status.rate,
        muted: status.isMuted,
        volume: status.volume,
        shouldCorrectPitch: status.shouldCorrectPitch,
        isPlaybackAllowed: true,
      });
    } else {
      this.setState({
        soundDuration: null,
        soundPosition: null,
        isPlaybackAllowed: false,
      });
    }
  };

  private _updateScreenForRecordingStatus = (status: Audio.RecordingStatus) => {
    if (status.canRecord) {
      this.setState({
        isRecording: status.isRecording,
        recordingDuration: status.durationMillis,
      });
    } else if (status.isDoneRecording) {
      this.setState({
        isRecording: false,
        recordingDuration: status.durationMillis,
      });
      if (!this.state.isLoading) {
        this._stopRecordingAndEnablePlayback();
      }
    }
  };

  private async _stopPlaybackAndBeginRecording() {
    this.setState({
      isLoading: true,
    });
    if (this.sound !== null) {
      await this.sound.unloadAsync();
      this.sound.setOnPlaybackStatusUpdate(null);
      this.sound = null;
    }
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
      staysActiveInBackground: true,
    });
    if (this.recording !== null) {
      this.recording.setOnRecordingStatusUpdate(null);
      this.recording = null;
      console.log("recording should be null now :)))");
    }

    const recording = new Audio.Recording();
    await recording.prepareToRecordAsync(this.recordingSettings);
    recording.setOnRecordingStatusUpdate(this._updateScreenForRecordingStatus);

    this.recording = recording;
    await this.recording.startAsync();
    this.setState({
      isLoading: false,
    });
  }

  private async _stopRecordingAndEnablePlayback() {
    this.setState({
      isLoading: true,
    });
    if (!this.recording) {
      return;
    }
    try {
      await this.recording.stopAndUnloadAsync();
    } catch (error) {
      if (error.code === "E_AUDIO_NODATA") {
        console.log(
          `Stop was called too quickly, no data has yet been received (${error.message})`
        );
      } else {
        console.log("STOP ERROR: ", error.code, error.name, error.message);
      }
      this.setState({
        isLoading: false,
      });
      return;
    }
    const info = await FileSystem.getInfoAsync(this.recording.getURI() || "");
    console.log(`FILE INFO: ${JSON.stringify(info)}`);
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
      staysActiveInBackground: true,
    });
    const { sound, status } = await this.recording.createNewLoadedSoundAsync(
      {
        isLooping: true,
        isMuted: this.state.muted,
        volume: this.state.volume,
        rate: this.state.rate,
      },
      this._updateScreenForSoundStatus
    );
    this.sound = sound;
    this.setState({
      isLoading: false,
    });
  }

  private _onRecordPressed = () => {
    console.log("pressed recording");
    if (this.state.isRecording) {
      this._stopRecordingAndEnablePlayback();
    } else {
      this._stopPlaybackAndBeginRecording();
    }
  };

  private _onPlayPausePressed = () => {
    if (this.sound != null) {
      if (this.state.isPlaying) {
        this.sound.pauseAsync();
      } else {
        this.sound.playAsync();
      }
    }
  };


  private _getMMSSFromMillis(millis: number) {
    const totalSeconds = millis / 1000;
    const seconds = Math.floor(totalSeconds % 60);
    const minutes = Math.floor(totalSeconds / 60);

    const padWithZero = (number: number) => {
      const string = number.toString();
      if (number < 10) {
        return "0" + string;
      }
      return string;
    };
    return padWithZero(minutes) + ":" + padWithZero(seconds);
  }

  private _getPlaybackTimestamp() {
    if (
      this.sound != null &&
      this.state.soundPosition != null &&
      this.state.soundDuration != null
    ) {
      return `${this._getMMSSFromMillis(
        this.state.soundPosition
      )} / ${this._getMMSSFromMillis(this.state.soundDuration)}`;
    }
    return "";
  }

  private _getRecordingTimestamp() {
    if (this.state.recordingDuration != null) {
      return `${this._getMMSSFromMillis(this.state.recordingDuration)}`;
    }
    return `${this._getMMSSFromMillis(0)}`;
  }

  private addRecording() {
  /*
  the uri is a file on the phone that has the recording. 
  TODO: find a way to get the recording & correspond with the bike & make it accessible for everyone else
  */
   const uri =  this.recording.getURI() || "";
  }

  private rerecord() {
    if(this.sound) {
      this.sound = null;
      this.setState({
      isLoading: false,
      isPlaybackAllowed: false,
      muted: false,
      soundPosition: null,
      soundDuration: null,
      recordingDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isRecording: false,
      fontLoaded: false,
      shouldCorrectPitch: true,
      volume: 1.0,
      rate: 1.0,
    });
    }
  }
  render() {
    const recordingTimestamp = this._getRecordingTimestamp();
    if (!this.state.hasRecordingPermissions) {
      return (
        <View>
          <Text
            style={{
              textAlign: "center",
              width: "80%",
              alignSelf: "center",
              top: 30,
            }}
          >
            You must enable audio recording permissions in order to use this
            functionality.
          </Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        {this.sound ? (
          <View>
            <Text style={styles.title}>Playback</Text>
            <Slider
              trackImage={require("../assets/playback.png")}
              thumbImage={require("../assets/thumb.png")}
              style={styles.slider}
              value={this.state.soundPosition / this.state.soundDuration}
            />
            <Text style={styles.playbackTimestamp}>{this._getPlaybackTimestamp()}</Text>
            <View style={styles.spacer}/>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => {
                this._onPlayPausePressed();
              }}
            >
              <Ionicons
                name={this.state.isPlaying ? "pause-outline" : "play-outline"}
                size={30}
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.submit} onPress={()=>{this.addRecording()}}>
              <Text style={styles.submitText}>send it in!</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.rerecord()}}>
              <Text style={styles.rerecord}>Not what your looking for? Record a new entry.</Text>
            </TouchableOpacity>
          </View>
         ) : (
          <View>
            <Text style={styles.title}>Record new entry</Text>
            <Slider
              trackImage={require("../assets/playback.png")}
              thumbImage={require("../assets/thumb.png")}
              style={styles.slider}
              value={this.state.recordingDuration / 10000}
            />
            <Text style={styles.timestamp}>{recordingTimestamp}</Text>
            <View style={styles.spacer} />
            <TouchableOpacity
              style={styles.icon}
              onPress={() => {
                this._onRecordPressed();
              }}
            >
              <Ionicons
                name={
                  this.state.isRecording ? "stop-circle-outline" : "mic-outline"
                }
                size={25}
              />
            </TouchableOpacity>
          </View>
        )} 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
    top: 20,
    width: '100%',
    textAlign: 'center',
    color: MAIN_COLOR,
  },
  slider: {
    alignSelf: "center",
    top: 30,
    width: "70%",
    marginRight: 10,
  },
  timestamp: {
    alignSelf: "center",
    width: "100%",
    textAlign: "right",
    paddingRight: 10,
  },
  spacer: {
    height: 30,
  },
  icon: {
    alignSelf: "center",
  },
  playbackTimestamp: {
    alignSelf: "center",
    width: "50%",    
    textAlign: 'center',
    top: 20,
    fontSize:20
  },
  rerecord: {
    width: '70%',
    alignSelf: 'flex-end',
    marginLeft: 20,
    marginTop: 10,
    padding: 10,
    color: MAIN_COLOR,
    textAlign: 'right',
  },
  submit: {
    backgroundColor: MAIN_COLOR,
    borderRadius: 5,
    width: '30%',
    aspectRatio: 2,
    alignSelf: 'flex-end',
    marginRight: 25,
    marginTop: 10,
    justifyContent: 'center'
  },
  submitText: {
    color: "white",
    fontWeight: '800',
    textAlign: 'center',
    width: '100%'
  },
});
