import React, { useState } from "react";
import {
  Image,
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Button,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import * as Speech from "expo-speech";
import { Audio } from "expo-av";

export default function Bike({ route, navigation }) {
  const [speak, setSpeak] = useState(false);
  // const [played, setPlayed] = useState(false);
  const { image, title, subtitle, description, mp3 } = route.params;
  const [sound, setSound] = useState();
  //const play = async () => {
  // setSpeak(!speak);
  // const speaking = await Speech.isSpeakingAsync();
  // if (!speaking) {
  //   Speech.speak(description, {
  //     onDone: () => {
  //       setSpeak(false);
  //       setPlayed(false);
  //     },
  //   });
  // }
  // if (speak && !played) {
  //   Speech.speak(description, {
  //     onDone: () => {
  //       console.warn("done");
  //       setSpeak(false);
  //       setPlayed(false);
  //     },
  //   });
  //   setPlayed(true);
  // } else if (speak && played) {
  //   Speech.resume();
  //   setPlayed(true);
  // } else if (!speak) {
  //   setPlayed(true);
  //   Speech.pause();
  // }
  //};

  async function playSound() {
    setSpeak(!speak);
    if (!sound) {
      const { sound } = await Audio.Sound.createAsync(require("../mp3/13.mp3"));
      setSound(sound);
      await sound.playAsync();
    } else if (sound && speak) {
      await sound.playAsync();
    } else if (sound && !speak) {
      await sound.pauseAsync();
    }
  }

  React.useEffect(() => {
    return sound
      ? () => {
          console.log("Unloading Sound");
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  return (
    <ScrollView style={styles.container}>
      <Image source={image} style={styles.bike} />
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.subtitle}>{subtitle}</Text>
      <View style={styles.audioContainer}>
        <Image source={require("../assets/playback.png")} />
        <View style={styles.controlsContainer}>
          <Ionicons
            name={"play-back-outline"}
            size={23}
            style={styles.controls}
          />
          <TouchableOpacity
            onPress={() => {
              playSound();
            }}
          >
            <Ionicons
              name={speak ? "play-outline" : "pause-outline"}
              size={28}
              style={styles.controls}
            />
          </TouchableOpacity>
          <Ionicons
            name="play-forward-outline"
            size={23}
            style={styles.controls}
          />
        </View>
      </View>
      <Text style={styles.subheading}>Description</Text>
      <Text style={styles.description}>{description}</Text>
      <View style={{ height: 60 }}></View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Record");
        }}
      >
        <Text style={styles.button}>Record</Text>
      </TouchableOpacity>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    left: "10%",
    top: 10,
  },
  subtitle: {
    left: "10%",
    top: 10,
  },
  bike: {
    width: "80%",
    aspectRatio: 3 / 2,
    alignSelf: "center",
    height: undefined,
  },
  audioContainer: {
    top: 30,
    width: "100%",
    alignItems: "center",
  },
  controlsContainer: {
    display: "flex",
    flexDirection: "row",
  },
  controls: {
    marginTop: 8,
    marginLeft: 12,
    marginRight: 12,
  },
  subheading: {
    fontSize: 18,
    fontWeight: "500",
    left: "10%",
    top: 35,
  },
  description: {
    left: "10%",
    top: 40,
    width: "83%",
    textAlign: "left",
  },
  button: {
    color: "#FC6351",
    fontWeight: "500",
    paddingBottom: 20,
    width: "90%",
    textAlign: "right",
  },
});
