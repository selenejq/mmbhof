import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, Button, SafeAreaView } from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { Ionicons } from "@expo/vector-icons";
import axios from "axios";
import { REACT_APP_SECRET_KEY, REACT_APP_URL } from "@env";

// Variables for api key and url - Angela
const secretKey = REACT_APP_SECRET_KEY;
const apiUrl = REACT_APP_URL;

export default function Scan(props) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [bounds, setBounds] = useState(null);
  const [corners, setCorners] = useState(null);
  // Variables for bike data - Angela 
  const [bikeData, setBikeData] = useState({});

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
      // Calls function to get bike data from api - Angela
      getBikeData();
    })();
  }, []);

  // Function to get json file bike data with axios - Angela

  const getBikeData = async () => {
    const response = await axios.get(apiUrl, {
      headers: {
        "secret-key": secretKey,
      },
    });
    setBikeData(response.data);
  };

  const handleBarCodeScanned = ({ bounds, data }) => {
    setScanned(true);
    setBounds(bounds);
    setTimeout(() => {
      setScanned(false);
      // Takes specific exhibit data by indexing json file 
      // with number from qr code scanned - Angela 
      props.navigation.navigate("Bike", {
        mp3: `mp3/${data}.mp3`,
        image: { uri: bikeData[data].image },
        title: bikeData[data].title,
        subtitle: bikeData[data].type,
        description: bikeData[data].description,
      });
    }, 1500);
  };

  if (hasPermission === null) {
    console.warn("null perms");
    return (
      <SafeAreaView>
        <Text>Requesting for camera permission</Text>
      </SafeAreaView>
    );
  }
  if (hasPermission === false) {
    return (
      <SafeAreaView>
        <Text>No access</Text>
      </SafeAreaView>
    );
  }

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? null : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && (
        <Ionicons
          size={bounds.size.height + 5}
          color={"#FC6351"}
          name="scan-outline"
          style={(styles.icon, { top: bounds.origin.y, left: bounds.origin.x })}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    backgroundColor: "yellow",
  },
  icon: {
    position: "absolute",
  },
});
