# Contributing

This app is written in React/TypeScript and developed with [Expo](https://expo.dev/). You can run Expo native or use it as a plugin to your favorite editor (Kate, VSCode, Sublime, vim, etc).

# Steps to Success

1. Install [Expo](https://expo.dev/).
1. Git pull.
1. Write code.
1. Send a merge request.
